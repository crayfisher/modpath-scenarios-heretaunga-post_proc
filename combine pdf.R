library(png)
library(magick)

folder <- "C:/PAWEL/R/modpath/tm_png1"
folder_test <- "C:/PAWEL/R/modpath/test"
files <- dir(folder, full.names = T)
files1 <- files[1:5]
i<-1
pdf(file = paste(folder_test,"1yr.pdf",sep="/"),
    onefile = T,
    paper = "a4r",
    compress = T,
    width = 11,
    height = 8)
    for (i in 1:length(files)){
      img1 <- files[i] %>% 
        image_read() %>% 
        image_resize(size1) %>% 
        image_convert("jpg")
      plot(img1)}

  

dev.off()

folder <- "C:/PAWEL/R/modpath/tm_png10"
files <- dir(folder, full.names = T)
pdf(file = paste(folder_test,"10yr.pdf",sep="/"),
    onefile = T,
    paper = "a4r",
    compress = T,
    width = 11,
    height = 8)

pb = txtProgressBar(min = 0, max = length(files), initial = 0) 
for (i in 1:length(files)){
  setTxtProgressBar(pb,i)
  img1 <- files[i] %>% 
    image_read() %>% 
    image_resize(size1) %>% 
    image_convert("jpg")
  plot(img1)}



dev.off()

#stacking images
folder <- "C:/PAWEL/R/modpath/tm_png1"



folder_test <- "C:/PAWEL/R/modpath/test"
files <- dir(folder, full.names = T)
files_sc1 <- grep("tm_[1|10]_1_",files,value = T)
files_sc2 <- grep("tm_[1|10]_2_",files,value = T)
files_sc3 <- grep("tm_[1|10]_3_",files,value = T)
files_sc4 <- grep("tm_[1|10]_4_",files,value = T)
i=1

size <- 500
size1 <- paste(size,size,sep="x")
pb = txtProgressBar(min = 0, max = length(files), initial = 0) 

for (i in 1:length(files_sc1)){
  setTxtProgressBar(pb,i)
  img1 <-files_sc1[i] %>% 
    image_read() %>% 
    image_trim() %>% 
    image_resize(size1) %>% 
    image_border( "white", "2x2")
  img2 <- files_sc2[i] %>% 
    image_read() %>% 
    image_trim() %>% 
    image_resize(size1)%>% 
    image_border( "white", "2x2")
  img3 <- files_sc3[i] %>% 
    image_read() %>% 
    image_trim() %>% 
    image_resize(size1)%>% 
    image_border( "white", "2x2") 
  img4 <- files_sc4[i] %>% 
    image_read() %>% 
    image_trim() %>% 
    image_resize(size1)%>% 
    image_border( "white", "2x2")
  imgA <- image_append(c(img1,img2))
  #print(imgA)
  imgB <- image_append(c(img3,img4))
  #print(imgB)
  imgC <- image_append(c(imgA,imgB),stack = T) %>% 
    image_border( "white", "20x20") %>% 
    image_convert("pdf") 

  
  if(i == 1){img_list <- imgC}else{img_list <- c(img_list,imgC)} 

}



image_write(img_list,"1yr_new.pdf")


#same for 10 year

folder <- "C:/PAWEL/R/modpath/tm_png10"

folder_test <- "C:/PAWEL/R/modpath/test"
files <- dir(folder, full.names = T)
files_sc1 <- grep("tm_(1|10)_1_",files,value = T)
files_sc2 <- grep("tm_(1|10)_2_",files,value = T)
files_sc3 <- grep("tm_(1|10)_3_",files,value = T)
files_sc4 <- grep("tm_(1|10)_4_",files,value = T)
i=1

size <- 500
s
size1 <- paste(size,size,sep="x")
pb = txtProgressBar(min = 0, max = length(files_sc1), initial = 0,style = 3) 

for (i in 1:length(files_sc1)){
  setTxtProgressBar(pb,i)
  img1 <-files_sc1[i] %>% 
    image_read() %>% 
    image_trim() %>% 
    image_resize(size1) %>% 
    image_border( "white", "2x2")
  img2 <- files_sc2[i] %>% 
    image_read() %>% 
    image_trim() %>% 
    image_resize(size1)%>% 
    image_border( "white", "2x2")
  img3 <- files_sc3[i] %>% 
    image_read() %>% 
    image_trim() %>% 
    image_resize(size1)%>% 
    image_border( "white", "2x2") 
  img4 <- files_sc4[i] %>% 
    image_read() %>% 
    image_trim() %>% 
    image_resize(size1)%>% 
    image_border( "white", "2x2")
  imgA <- image_append(c(img1,img2))
  #print(imgA)
  imgB <- image_append(c(img3,img4))
  #print(imgB)
  imgC <- image_append(c(imgA,imgB),stack = T) %>% 
    image_border( "white", "20x20") %>% 
    image_convert("pdf") 
  
  if(i == 1){img_list <- imgC}else{img_list <- c(img_list,imgC)} 
  
}



image_write(img_list,"10 year.pdf")




A4x <-11.69
Px <- 1048
dpi <- Px / A4x

#tests
img_test <- image_append(c(imgA,imgB),stack = T) %>% 
  image_border( "white", "20x20") %>% 
  image_convert("pdf") 
image_write(imgC,"test8.pdf",density = 90)
