library(tmap)
library(tmaptools)
library(tidyverse)
library(sf)
library(sp)
library(OpenStreetMap)
library(stringr)

proj4 <- "+proj=tmerc +lat_0=0 +lon_0=173 +k=0.9996 +x_0=1600000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"
layer <- 1
fn1 <- paste("point_pe_L",layer,".dat",sep="")
list_par <- dir("C:/PAWEL/MODELLING/HPM/hpm035/modpath scenarios/pest/model_real/",
                recursive = T,
                pattern = fn1)
list_par1 <- dir("C:/PAWEL/MODELLING/HPM/hpm035/modpath scenarios/pest/model_real/",
                 recursive = T,
                 pattern = fn1,
                 full.names = T)

list_real <- grep("[0-9]+/",list_par,value = T)
list_real <- as.integer(as.vector(str_match(list_real,"[0-9]+")))

i=1

df_comb1 <- NULL
df_range <- read_table2(list_par1[1],col_names = c("id","x","y","L","val")) %>% 
  st_as_sf(coords = c("x","y"),crs = 2193)
osm_map3 <- read_osm(df_range,type = "osm")


for (i in 1:length(list_par1)){
  
  
  tm_name <- paste("tm_py_",layer,realisation,sep="_")
  tm_namepng <- paste(tm_name,".png",sep = "")
  tm_title <- paste("porosity map for \nlayer:",layer,"\nrealisation:",realisation,sep=" ")
  
  
  
   df<- read_table2(list_par1[i],col_names = c("id","x","y","L","val")) %>% 
    mutate(real = list_real[i]) %>% 
    st_as_sf(coords = c("x","y"),crs = 2193)
  
  tm<- qtm(osm_map3)+
    tm_shape(df)+
    tm_dots(col = "val",
            size =4,
            palette = "-Spectral",
            n = 10,
            title = tm_title)+
    tm_scale_bar()+
    tm_compass(position = c("right","top"))
  tm
  
  
  

  tmap_save(tm1,tm_name1png)
  
  
}