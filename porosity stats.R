library(tmap)
library(tmaptools)
library(tidyverse)
library(sf)
library(sp)
library(OpenStreetMap)
library(stringr)

list_par <- dir("C:/PAWEL/MODELLING/HPM/hpm035/modpath scenarios/pest/model_real/",
                recursive = T,
                pattern = "point_pe_L2.dat")
list_par1 <- dir("C:/PAWEL/MODELLING/HPM/hpm035/modpath scenarios/pest/model_real/",
                recursive = T,
                pattern = "point_pe_L2.dat",
                full.names = T)

list_real <- grep("[0-9]+/",list_par,value = T)
list_real <- as.integer(as.vector(str_match(list_real,"[0-9]+")))

i=1

df_comb1 <- NULL
for (i in 1:length(list_par1)){
  
  
  df <- read_table2(list_par1[i],col_names = c("id","x","y","L","val")) %>% 
    mutate(real = list_real[i])
  
  df_comb1 <- bind_rows(df_comb1,df)
  
}

load("cature1-10.rdata")
capture10_sf <- st_as_sf(capture10)

pe_spatial_1 <- df_comb1 %>% 
  st_as_sf(coords = c("x","y"),crs = 2193)
  




pe_spatial_1_sel_stat <- pe_spatial_1_sel %>% 
  group_by(id) %>% 
  summarise(mean = mean(val),
            min=min(val),
            max = max(val),
            Q10 = quantile(val,probs = 0.1),
            Q50 = quantile(val,probs = 0.5),
            Q90 = quantile(val,probs = 0.9))

pe_spatial_1_sel_stat2 <- pe_spatial_1_sel %>% 
  st_set_geometry(NULL) %>% 
  summarise(mean = mean(val),
            min=min(val),
            max = max(val),
            Q10 = quantile(val,probs = 0.1),
            Q50 = quantile(val,probs = 0.5),
            Q90 = quantile(val,probs = 0.9))





write_csv(pe_spatial_1_sel_stat2,"pe_spatial_2_sel_stat2.csv")


