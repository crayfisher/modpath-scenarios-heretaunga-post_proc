library(tmap)
library(tmaptools)
library(tidyverse)
library(sf)
library(sp)
library(OpenStreetMap)
library(raster)

proj4 <- "+proj=tmerc +lat_0=0 +lon_0=173 +k=0.9996 +x_0=1600000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"
ext2 <- extent(c(1915200, 1936100,5598300,5612400))
ras <- raster(ext = ext2,resolution = 100,crs = proj4)

#this is just a patch to wait for gw model to finish
#Sys.sleep(21600)

file_lst <- dir("C:/PAWEL/MODELLING/HPM/hpm035/modpath scenarios/pest",
                full.names = F,
                recursive = T,
                pattern = "mp1_[0-9]+_[0-9]+\\.timeseries",
                include.dirs = T)
file_lst <- gsub("sc[0-9]-set[0-9]/","", file_lst)



file_lst1 <- dir("C:/PAWEL/MODELLING/HPM/hpm035/modpath scenarios/pest",
                 full.names = T,
                 recursive = T,
                 pattern = "mp1_[0-9]+_[0-9]+\\.timeseries",
                 include.dirs = T)



#file_lst <- dir("timesr",full.names = T)
#file_lst1<- dir("timesr",full.names = F)

scens <- file_lst1 %>% 
  as.tibble() %>% 
  separate(value,sep = "_|\\.",into = as.character(seq(1,4,1))) %>% 
  dplyr::select(2,3) %>% 
  rename(scen =`2`, realisation = `3`) %>% 
  group_by(scen) %>% 
  summarise(count = n()) %>% 
  filter(as.integer(scen) <5)
  


read_timeseries <- function(ts_file){
  offset <- c(1891460,5594400) 
  col_names <- c("Time_Point_Index","Cumulative_Time_Step","Tracking_Time","Sequence_number","Particle_Group","Particle_ID","Cell_number",
                 "Local_x","Local_y","Local_z","Global_x","Global_y","Global_z","Layer")
  colnames2 <- c("x", "y", "z", "time", "particleid")
  
  ts <- read_table2(ts_file,skip = 3,col_names = F)
  
  colnames(ts) <- col_names
  ts2 <-   ts  %>% 
    dplyr::select(Global_x,Global_y,Global_z,Tracking_Time,Particle_ID) %>% 
    mutate(Global_x = offset[1]+ Global_x,
           Global_y = offset[2] + Global_y)
  
  colnames(ts2) <- colnames2 
  return(ts2)
}


sc1i = 1
sc2i = 1
sc3i = 1
sc4i = 1

sc1i10 = 1
sc2i10 = 1
sc3i10 = 1
sc4i10 = 1


i=1

ras1yr_cum_sc1 <- ras
ras1yr_cum_sc2 <- ras
ras1yr_cum_sc3 <- ras
ras1yr_cum_sc4 <- ras
ras10yr_cum_sc1 <- ras
ras10yr_cum_sc2 <- ras
ras10yr_cum_sc3 <- ras
ras10yr_cum_sc4 <- ras
ras10yr_cum <- res
#this is to combine to raster
#
close(pb2)
pb2 = txtProgressBar(min = 0, max = length(file_lst1), initial = 0, char = "=",
                     width = NA, title = "df", style = 2, file = "")
for (i in 1:length(file_lst1)){
  setTxtProgressBar(pb2, i)
  df <- read_timeseries(file_lst1[i])
  scenario <- grep("mp",file_lst[i],value = T)
  str <- unlist(strsplit(file_lst[i],"(\\.|_)"))
  scenario <- as.integer(str[2])
  realisation <- as.integer(str[3])
  df <- df %>% 
    mutate(sc = scenario,
           real = realisation) %>% 
    group_by(particleid) %>% 
    mutate(release_time = first(time),
           time_from_release = time - release_time) %>% 
    ungroup()
  df1 <- df %>% dplyr::select(x,y,time_from_release,release_time) %>% 
    mutate(dummy = 1)
  
  df_sp <- SpatialPointsDataFrame(coords = dplyr::select(df1,x,y), data = df1,
                                  proj4string = CRS(proj4))
 
  
  ras_10yr<- rasterize(df_sp,ras,fun = "max",na.rm =T,field = "dummy")
  ras_10yr[is.na(ras_10yr)] <- 0
  
  #plot(ras10yr)
  df_1yr <- df1 %>% 
    filter(time_from_release <= 365)
  
  df_sp_1yr <- SpatialPointsDataFrame(coords = dplyr::select(df_1yr,x,y), 
                                      data = df_1yr,
                                      proj4string = CRS(proj4))
  
  ras_1yr<- rasterize(df_sp_1yr,
                      ras,fun = "max",
                      field = "dummy")
  ras_1yr[is.na(ras_1yr)] <- 0
 # plot(ras_1yr)
  ras_name1 = paste("ras10yr",scenario,realisation,".rdata",sep="_")
  ras_name2 = paste("ras01yr",scenario,realisation,".rdata",sep="_")
  save(ras_1yr,file = ras_name2)
  save(ras_10yr,file = ras_name1)
  
  
  if(scenario == 1){
        sci <- sc1i
        ras1yr_cum <- ras1yr_cum_sc1
        if(sci == 1){
          ras1yr_cum <- ras_1yr
          }else {
          ras1yr_cum <- ras1yr_cum + ras_1yr}
        sci = sci + 1
        
        sc1i <- sci
        ras1yr_cum_sc1 <- ras1yr_cum
  } else if (scenario == 2) {
        sci <- sc2i
        ras1yr_cum <- ras1yr_cum_sc2
        
        if(sci == 1){
          ras1yr_cum <- ras_1yr
        }else {
          ras1yr_cum <- ras1yr_cum + ras_1yr}
        sci = sci + 1
        
        sc2i <- sci
        ras1yr_cum_sc2 <- ras1yr_cum    
  } else if (scenario == 3) {
        sci <- sc3i
        ras1yr_cum <- ras1yr_cum_sc3
        
        if(sci == 1){
          ras1yr_cum <- ras_1yr
        }else {
          ras1yr_cum <- ras1yr_cum + ras_1yr}
        
        sci = sci + 1
        
        sc3i <- sci
        ras1yr_cum_sc3 <- ras1yr_cum    
        
  } else if (scenario == 4) {
        sci <- sc4i
        ras1yr_cum <- ras1yr_cum_sc4
        
        if(sci == 1){
          ras1yr_cum <- ras_1yr
        }else {
          ras1yr_cum <- ras1yr_cum + ras_1yr}
        
        sci = sci + 1
        sc4i <- sci
        ras1yr_cum_sc4 <- ras1yr_cum    
  }

  
  #combine 10 yr
  if(scenario == 1){
    sci <- sc1i10
    ras10yr_cum <- ras10yr_cum_sc1
    if(sci == 1){
      ras10yr_cum <- ras_10yr
    }else {
      ras10yr_cum <- ras10yr_cum + ras_10yr}
    sci = sci + 1
    
    sc1i10 <- sci
    ras10yr_cum_sc1 <- ras10yr_cum
  } else if (scenario == 2) {
    sci <- sc2i10
    ras10yr_cum <- ras10yr_cum_sc2
    
    if(sci == 1){
      ras10yr_cum <- ras_10yr
    }else {
      ras10yr_cum <- ras10yr_cum + ras_10yr}
    sci = sci + 1
    
    sc2i10 <- sci
    ras10yr_cum_sc2 <- ras10yr_cum    
  } else if (scenario == 3) {
    sci <- sc3i10
    ras10yr_cum <- ras10yr_cum_sc3
    
    if(sci == 1){
      ras10yr_cum <- ras_10yr
    }else {
      ras10yr_cum <- ras10yr_cum + ras_10yr}
    
    sci = sci + 1
    
    sc3i10 <- sci
    ras10yr_cum_sc3 <- ras10yr_cum    
  } else if (scenario == 4) {
    sci <- sc4i10
    ras10yr_cum <- ras10yr_cum_sc4
    
    if(sci == 1){
      ras10yr_cum <- ras_10yr
    }else {
      ras10yr_cum <- ras10yr_cum + ras_10yr}
    
    sci = sci + 1
    sc4i10 <- sci
    ras10yr_cum_sc4 <- ras10yr_cum    
  }
  
  
  
   
  #basic progress
  print(i)
}
 
plot(ras1yr_cum_sc3)
 
save(ras1yr_cum_sc4,ras1yr_cum_sc3,ras1yr_cum_sc2,ras1yr_cum_sc1,file = "ras1yr_cum_sci.rdata")
save(ras10yr_cum_sc4,ras10yr_cum_sc3,ras10yr_cum_sc2,ras10yr_cum_sc1,file = "ras10yr_cum_sci.rdata")


ras1yr_cum_sc1_p <- ras1yr_cum_sc1/107
ras1yr_cum_sc2_p <- ras1yr_cum_sc2/107
ras1yr_cum_sc3_p <- ras1yr_cum_sc3/107
ras1yr_cum_sc4_p <- ras1yr_cum_sc4/107

ras10yr_cum_sc1_p <- ras10yr_cum_sc1/107
ras10yr_cum_sc2_p <- ras10yr_cum_sc2/107
ras10yr_cum_sc3_p <- ras10yr_cum_sc3/107
ras10yr_cum_sc4_p <- ras10yr_cum_sc4/107








ras1yr_cum_cum = max(ras1yr_cum_sc1_p,ras1yr_cum_sc2_p,ras1yr_cum_sc3_p,ras1yr_cum_sc4_p)*100
ras10yr_cum_cum = max(ras10yr_cum_sc1_p,ras10yr_cum_sc2_p,ras10yr_cum_sc3_p,ras10yr_cum_sc4_p)*100

plot(ras10yr_cum_cum)
plot(ras1yr_cum_cum)
save(ras10yr_cum_cum,ras1yr_cum_cum,file = "ras10_1yr_cum_cum.rdata")



zones <- st_read("gis/WaterSourceProtectionZones.shp")
plot(zones)
spz2 <- zones %>% 
  filter(Layer == "SPZ 2")
plot(spz2)

wells <- st_read("gis/WaterSourceProtectionWells.shp")



ras1yr_cum_cum[ras1yr_cum_cum == 0] <- NA
ras10yr_cum_cum[ras10yr_cum_cum == 0] <- NA
osm_map <- read_osm(ras1yr_cum_cum,type = "osm")
tm_pr <- qtm(osm_map)+
  tm_shape(ras1yr_cum_cum)+
  tm_raster("layer",alpha =0.5,
            palette = "-Spectral",
            breaks = seq(0,100,10),
            title = "probability of 1 year capture zone")+
  tm_shape(spz2)+
  tm_polygons(alpha = 0.4,
              lwd =1.5 )+
  tm_shape(wells)+
  tm_dots()+
  tm_scale_bar()+
  tm_compass(position = c("right","top"))
tm_pr
tmap_save(tm_pr,"1yr_ras.png")

osm_map10 <- read_osm(ras10yr_cum_cum,type = "osm")

tm_pr10 <- qtm(osm_map10)+
  tm_shape(ras10yr_cum_cum)+
  tm_raster("layer",alpha =0.5,
            palette = "-Spectral",
            breaks = seq(0,100,10),
            title = "probability of 10 year capture zone \nequivalent to very conservative 10x smaller porosity")+
  tm_shape(spz2)+
  tm_polygons(alpha = 0.4,
              lwd =1.5 )+
  tm_shape(wells)+
  tm_dots()+
  tm_scale_bar()+
  tm_compass(position = c("right","top"))
tm_pr10
tmap_save(tm_pr10,"10yr_ras.png")

load(file = "ras10_1yr_cum_cum.rdata")

ras10yr_cum_cum_90 <- ras10yr_cum_cum
ras10yr_cum_cum_90[ras10yr_cum_cum_90 < 10] <- NA
ras10yr_cum_cum_90[ras10yr_cum_cum_90 > 10] <- 1
plot(ras10yr_cum_cum_90)

capture10 <- rasterToPolygons(ras10yr_cum_cum_90,dissolve = T)
plot(capture10)

ras1yr_cum_cum_90 <- ras1yr_cum_cum
ras1yr_cum_cum_90[ras1yr_cum_cum_90 < 10] <- NA
ras1yr_cum_cum_90[ras1yr_cum_cum_90 > 10] <- 1
plot(ras1yr_cum_cum_90)

capture1 <- rasterToPolygons(ras1yr_cum_cum_90,dissolve = T)
plot(capture1)

save(capture1,capture10,file = "cature1-10.rdata")


