
library(tidyverse)
library(sf)
library(sp)
library(raster)

read_timeseries <- function(ts_file){
  offset <- c(1891460,5594400) 
  col_names <- c("Time_Point_Index","Cumulative_Time_Step","Tracking_Time","Sequence_number","Particle_Group","Particle_ID","Cell_number",
                 "Local_x","Local_y","Local_z","Global_x","Global_y","Global_z","Layer")
  colnames2 <- c("x", "y", "z", "time", "particleid")
  
  ts <- read_table2(ts_file,skip = 3,col_names = F)
  
  colnames(ts) <- col_names
  ts2 <-   ts  %>% 
    dplyr::select(Global_x,Global_y,Global_z,Tracking_Time,Particle_ID) %>% 
    mutate(Global_x = offset[1]+ Global_x,
           Global_y = offset[2] + Global_y)
  
  colnames(ts2) <- colnames2 
  return(ts2)
}

seti <- 7
leni <- 15
print(seti)

ran <- c((leni*seti -leni+1),(seti)*(leni))
proj4 <- "+proj=tmerc +lat_0=0 +lon_0=173 +k=0.9996 +x_0=1600000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"
ext2 <- extent(c(1915200, 1936100,5598300,5612400))
ras <- raster(ext = ext2,resolution = 100,crs = proj4)



file_lst <- dir("C:/PAWEL/MODELLING/HPM/hpm035/modpath scenarios/pest",
                full.names = F,
                recursive = T,
                pattern = "mp1_4_[0-9]+\\.timeseries",
                include.dirs = T)
file_lst <- gsub("sc[0-9]-set[0-9]/","", file_lst)



file_lst1 <- dir("C:/PAWEL/MODELLING/HPM/hpm035/modpath scenarios/pest",
                full.names = T,
                recursive = T,
                pattern = "mp1_4+_[0-9]+\\.timeseries",
                include.dirs = T)

file_lst <- file_lst[ran[1]:ran[2]]
file_lst1 <- file_lst1[ran[1]:ran[2]]
print(file_lst)



sc1i = 1
sc2i = 1
sc3i = 1
sc4i = 1
i=1

ras1yr_cum_sc1 <- ras
ras1yr_cum_sc2 <- ras
ras1yr_cum_sc3 <- ras
ras1yr_cum_sc4 <- ras


i=1
#this is to combine to raster
for (i in 1:length(file_lst1)){
  print(i)
  print(file_lst[i])
  df <- read_timeseries(file_lst1[i])
  scenario <- grep("mp",file_lst1[i],value = T)
  str <- unlist(strsplit(file_lst1[i],"(\\.|_)"))
  scenario <- as.integer(str[2])
  realisation <- as.integer(str[3])
  if (scenario == 4) {
  
  
  
  
    df <- df %>% 
      mutate(sc = scenario,
             real = realisation) %>% 
      group_by(particleid) %>% 
      mutate(release_time = first(time),
             time_from_release = time - release_time) %>% 
      ungroup()
    df1 <- df %>% dplyr::select(x,y,time_from_release,release_time) %>% 
      mutate(dummy = 1)
    
    df_sp <- SpatialPointsDataFrame(coords = dplyr::select(df1,x,y), data = df1,
                                    proj4string = CRS(proj4))
    #ras10yr<- rasterize(df_sp,ras,fun = "min",na.rm =T,field = "time_from_release")
    #plot(ras10yr)
    df_1yr <- df1 %>% 
      filter(time_from_release <= 365)
    
    df_sp_1yr <- SpatialPointsDataFrame(coords = dplyr::select(df_1yr,x,y), 
                                        data = df_1yr,
                                        proj4string = CRS(proj4))
    
    ras_1yr<- rasterize(df_sp_1yr,
                        ras,fun = "max",
                        field = "dummy")
    ras_1yr[is.na(ras_1yr)] <- 0
    # plot(ras_1yr)
    #ras_name1 = paste("ras10yr",scenario,realisation,".rdata",sep="_")
    ras_name2 = paste("ras01yr",scenario,realisation,".rdata",sep="_")
    save(ras_1yr,file = ras_name2)
    #save(ras10yr,file = ras_name1)
  
  

    sci <- sc4i
    ras1yr_cum <- ras1yr_cum_sc4
    
    if(sci == 1){
      ras1yr_cum <- ras_1yr
    }else {
      ras1yr_cum <- ras1yr_cum + ras_1yr}
    
    sci = sci + 1
    sc4i <- sci
    ras1yr_cum_sc4 <- ras1yr_cum    
  }
  

  
}

save(ras1yr_cum_sc4,file = paste("ras1yr_cum_sc4",seti,".rdata",sep="_"))